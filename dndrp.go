// Package dndrp implements a Service that acts as a helpful RPG bot for
// DnD-style RPs
package dndrp

/*
TODO:

* Randomly-generated, fun error messages
*   Error messages would be configured in a json file

*/

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/matrix-org/go-neb/types"
	"github.com/matrix-org/gomatrix"
)

var (
	players    map[string]*Player
	items      map[string]*Item
	statusMsgs map[string]*StatusMsg
	service    *Service
)

// Configure service type
const ServiceType = "dndrp"

// TODO: Add config fields?
type Service struct {
	types.DefaultService
}

type StatusMsg struct {
	Description string   `json:"description"`
	StatusTexts []string `json:"statustexts"`
}

// getStatus grabs a random status string from the statusmsgs.json file and
// returns it as a message for the bot to say to the user
func getStatus(statusId string) string {
	// Get the correct StatusMsg object
	msg, ok := statusMsgs[statusId]

	if !ok {
		return fmt.Sprintln("Unknown status identifier: ", statusId, statusMsgs)
	}

	// Generate a random int within range
	rand.Seed(time.Now().Unix())
	index := rand.Intn(len(msg.StatusTexts))

	return msg.StatusTexts[index]
}

// Stats define a series of attributes that a player can have.
// They can also be modified by items or spells
// TODO: Be able to influence a player's stats in a simple format:
// !ch playername st:+1 in:-5
type Stats struct {
	Strength     int `json:"strength"`
	Constitution int `json:"constitution"`
	Dexterity    int `json:"dexterity"`
	Intelligence int `json:"intelligence"`
	Wisdom       int `json:"wisdom"`
	Charisma     int `json:"charisma"`
	Regression   int `json:"regression"`
	Incontinence int `json:"incontinence"`

	Bladder int `json:"bladder"`
	Bowels  int `json:"bowels"`
}

func (dst *Stats) addStats(src *Stats) interface{} {
	dst.Strength += src.Strength
	dst.Constitution += src.Constitution
	dst.Dexterity += src.Dexterity
	dst.Intelligence += src.Intelligence
	dst.Wisdom += src.Wisdom
	dst.Charisma += src.Charisma
	dst.Regression += src.Regression
	dst.Incontinence += src.Incontinence

	dst.Bladder += src.Bladder
	dst.Bowels += src.Bowels

	return nil
}

// Stats for each player
type Player struct {
	Name   string   `json:"name"`
	RoomId string   `json:"roomid"`
	Items  []string `json:"items"`

	BladderWarningGiven bool      `json:"bladderwarninggiven"`
	BowelWarningGiven   bool      `json:"bowelwarninggiven"`
	Diapers             []*Diaper `json:"diapers"`
	DiaperFullness	    int	      `json:"diaperfullness"`

	Stats *Stats `json:"stats"`

	IsBig bool `json:"isbig"`

	// Each slice index holds a string that is made up of a little's
	// roomId+userId
	Littles []string `json:"littles"`
}

// GetItems is a helper function that returns a slice of Item objects that
// the player has access to
func (p *Player) GetItems(itemSlice []*Item) {
	for _, itemname := range p.Items {
		itemSlice = append(itemSlice, items[itemname])
	}

	return
}

// RemoveItem is a helper function that removes a single instance of an item from
// a player's item list given only an item name
func (p *Player) RemoveItem(itemname string) {
	for index, value := range p.Items {
		if value == itemname {
			// Remove value at specific index from slice
			p.Items = append(p.Items[:index], p.Items[index+1:]...)
			return
		}
	}
}

// GetLittles is a helper function that returns a slice of Player objects that
// are considered to be the given player's littles
func (p *Player) GetLittles(littleSlice []*Player) {
	for _, littleId := range p.Littles {
		littleSlice = append(littleSlice, players[littleId])
	}

	return
}

// TODO: React to nickname updates

// DiaperThickness is a helper function that returns the total thickness of all
// of the given player's diapers as a sum representing the amount of
// centimeters
func (p Player) DiaperThickness() int {
	var totalThickness int = 0
	for _, v := range p.Diapers {
		totalThickness += v.Thickness
	}
	return totalThickness
}

// Diaper is a helper function that puts a diaper on a player
func (p *Player) Diaper(diaper *Diaper) interface{} {
	p.Diapers = append(p.Diapers, diaper)
	return message(p.Name, " is now wearing ", len(p.Diapers), " diaper(s) for ", p.DiaperThickness(), "cm of total padding!")
}

// increaseBladderBowels function augments the player's bladder and bowel
// levels while also warning them if they get too high.
func (p *Player) increaseBladderBowels() interface{} {
	// TODO: Make incontinence lower your threshold for wetting, as well as have only
	// a random chance to be warned about it
	// And whether !hold will fail or not
	p.Stats.Bladder += 3
	p.Stats.Bowels += 1

	wetMessThreshold := 100 - p.Stats.Incontinence/2
	warningThreshold := wetMessThreshold - 10

	rand.Seed(time.Now().Unix())
	randNum := rand.Intn(100)
	// Warn depending on random number
	shouldWarn := randNum > p.Stats.Incontinence

	fullMsg := ""

	if p.Stats.Bladder > wetMessThreshold {
		p.Stats.Bladder = 0
		p.BladderWarningGiven = false
		fullMsg += fmt.Sprintln(p.Name, getStatus("player_wet"))
	}
	if p.Stats.Bowels > wetMessThreshold {
		p.Stats.Bowels = 0
		p.BowelWarningGiven = false
		fullMsg += fmt.Sprintln(p.Name, getStatus("player_mess"))
	}

	if !p.BladderWarningGiven &&
		p.Stats.Bladder > warningThreshold {
		p.BladderWarningGiven = true
		if shouldWarn {
			fullMsg += fmt.Sprintln(p.Name, getStatus("player_wet_warn"))
		}
	}
	if !p.BowelWarningGiven &&
		p.Stats.Bowels > warningThreshold {
		p.BowelWarningGiven = true
		if shouldWarn {
			fullMsg += fmt.Sprintln(p.Name, getStatus("player_mess_warn"))
		}
	}

	if fullMsg != "" {
		return message(fullMsg)
	}

	return nil
}

// TODO: Temporary stats

// ConsumeItem will remove an item from a player's inventory and add the item's
// attributes to the player's own attributes
func (p *Player) ConsumeItem(item *Item) interface{} {
	if err := p.Stats.addStats(item.StatMods); err != nil {
		return message("Error: ", err)
	}
	return message(p.Name, " consumed the ", item.Name, "!")
}

// WetDiaper will empty the contents of a player's bowels into their diaper
func (p *Player) WetDiaper() {
	// Empty their bladder into their diaper
	p.DiaperFullness += p.Stats.Bladder

	// Mark their bladder as empty
	p.Stats.Bladder = 0
}

// MessDiaper will empty the contents of a player's bowels into their diaper
func (p *Player) MessDiaper() {
	// Empty their bowels into their diaper
	p.DiaperFullness += p.Stats.Bowels

	// Mark their bowels as empty
	p.Stats.Bowels = 0
}

// ListItems will return a formatted message that lists all currently known
// items for a specific user
func (p *Player) ListItems() interface{} {
	if len(p.Items) <= 0 {
		return message(p.Name, " has no items!")
	}

	fstr := "<p>Items for " + p.Name + ":</p><pre><code>"
	str := "Items for " + p.Name + ":\n```"
	first := true
	for _, itemName := range p.Items {
		if item, ok := items[itemName]; ok {
			// Eliminate extra spacing on Riot
			if first {
				fstr = fstr + item.Name
				first = false
			} else {
				fstr = fstr + "\n" + item.Name
			}
			str = str + "\n" + item.Name
		}
	}
	fstr = fstr + "\n</code>"
	str = str + "\n```"
	return hmessage(str, fstr)
}

// getPlayerByRoomAndName returns a pointer to the player object given a roomId
// and player name
func getPlayerByRoomAndName(roomId, name string) (*Player, bool) {
	for _, player := range players {
		if player.RoomId == roomId && player.Name == name {
			return player, true
		}
	}

	// Error, couldn't find player
	return nil, false
}

// registerPlayer will add a new player, as well as ensure that they specified
// whether they want to be a big or a little
func registerPlayer(roomId, userId, displayName string, messageText []string) interface{} {
	if len(messageText) < 1 {
		return message("Syntax is !play big|little.")
	}
	if messageText[0] != "big" && messageText[0] != "little" {
		return message("You must specify if you're a big or a little. ",
			"Syntax is !play big|little")
	}
	if _, ok := players[roomId+userId]; ok {
		return message(getStatus("player_already_playing"))
	}

	isbig := messageText[0] == "big"

	players[roomId+userId] = &Player{
		Stats: &Stats{
			Incontinence: 1,
		},
		RoomId: roomId,
		Name:   displayName,
		IsBig:  isbig,
	}

	if isbig {
		return message("You are now playing as a big, ", displayName, ". Have fun!")
	}
	return message("You are now playing as a little, ", displayName, ". Have fun!")
}

// listPlayers will print out all known players in the game session
func listPlayers(roomId string) string {
	var playernames []string
	for _, player := range players {
		if player.RoomId == roomId {
			playernames = append(playernames, player.Name)
		}
	}
	return strings.Join(playernames, ", ")
}

type Diaper struct {
	Thickness int    `json:"thickness"`
	StatMods  *Stats `json:"statmods"`
}

type Item struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Consumable  bool   `json:"consumable"`
	RoomId      string `json:"roomid"`
	StatMods    *Stats `json:"statmods"`
}

// processItem will take a string and create a new item object from it.
// If a certain item attribute is not specified, it will be set to the default
func processItem(attrs []string) (newItem *Item) {
	newItem = &Item{
		StatMods: &Stats{},
	}

	// To Lower item name
	newItem.Name = attrs[0]

	// Iterate through each of the arguments and assign their given values
	for _, arg := range attrs[1:] {
		keyvalue := strings.Split(arg, ":")
		if len(keyvalue) == 1 {
			newItem.Description = arg
			continue
		}
		switch strings.ToLower(keyvalue[0]) {
		case "consumable", "cons":
			newItem.Consumable, _ = strconv.ParseBool(keyvalue[1])
		case "strength", "str":
			newItem.StatMods.Strength, _ = strconv.Atoi(keyvalue[1])
		case "constitution", "con":
			newItem.StatMods.Constitution, _ = strconv.Atoi(keyvalue[1])
		case "dexterity", "dex":
			newItem.StatMods.Dexterity, _ = strconv.Atoi(keyvalue[1])
		case "intelligence", "int":
			newItem.StatMods.Intelligence, _ = strconv.Atoi(keyvalue[1])
		case "wisdom", "wis":
			newItem.StatMods.Wisdom, _ = strconv.Atoi(keyvalue[1])
		case "charisma", "cha":
			newItem.StatMods.Charisma, _ = strconv.Atoi(keyvalue[1])
		case "regression", "reg":
			newItem.StatMods.Regression, _ = strconv.Atoi(keyvalue[1])
		case "incontinence", "inc":
			newItem.StatMods.Incontinence, _ = strconv.Atoi(keyvalue[1])
		case "bladder", "bla":
			newItem.StatMods.Bladder, _ = strconv.Atoi(keyvalue[1])
		case "bowels", "bow":
			newItem.StatMods.Bowels, _ = strconv.Atoi(keyvalue[1])
		}
	}

	return
}

// listItems will return a formatted message that lists all currently known
// items
func listItems(roomId string) interface{} {
	fstr := "<p>Currently known items:</p><pre><code>"
	str := "Currently known items:\n```"
	first := true
	for _, item := range items {
		// Only show items for the current room
		if item.RoomId != roomId {
			continue
		}

		// Eliminate extra spacing on Riot
		if first {
			fstr = fstr + item.Name
			first = false
		} else {
			fstr = fstr + "\n" + item.Name
		}
		str = str + "\n" + item.Name
	}
	fstr = fstr + "\n</code>"
	str = str + "\n```"
	return hmessage(str, fstr)
}

func listCommands(commands []types.Command) interface{} {
	fstr := "<p>Commands:</p><pre><code>"
	str := "Commands:\n```"
	first := true
	for _, command := range commands {
		// Eliminate extra spacing on Riot
		commandText := strings.Join(command.Path, ", ")
		if first {
			fstr += commandText
			first = false
		} else {
			fstr += "\n" + commandText
		}
		str += "\n" + commandText
		str += "\n\t" + command.Help
		fstr += "\n\t" + command.Help

		str += "\n\t" + "Usage: " + command.Usage
		fstr += "\n\t" + "Usage: " + command.Usage
	}
	fstr += "\n</code>"
	str += "\n```"

	return hmessage(str, fstr)
}

// Commands
//   Currently supported:
//	 !echo
func (s *Service) Commands(cli *gomatrix.Client) []types.Command {
	return []types.Command{
		types.Command{
			Path:  []string{"echo"},
			Help:  "Make the bot say something!",
			Usage: "!echo I'm a silly butt!",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return message(strings.Join(args, " ")), nil
			},
		},
		types.Command{
			Path:  []string{"help"},
			Help:  "Print out this help text",
			Usage: "!help",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return listCommands(service.Commands(cli)), nil
			},
		},
		types.Command{
			Path:  []string{"play"},
			Help:  "Start playing!",
			Usage: "!play little|big",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				stateEvent, err := getStateEvent(cli, roomId, "m.room.member", userId)
				if err != nil {
					return message("Whoopsie! Error: ", err), nil
				}
				displayName := stateEvent["displayname"]
				return registerPlayer(roomId, userId, displayName, args), nil
			},
		},
		types.Command{
			Path:  []string{"stopplaying"},
			Help:  "End your play session. WARNING: This will delete your player data!",
			Usage: "!stopplaying",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				delete(players, roomId+userId)
				return message(getStatus("player_stops_playing")), nil
			},
		},
		types.Command{
			Path:  []string{"listplayers"},
			Help:  "Lists all people currently playing",
			Usage: "!listplayers",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return message(listPlayers(roomId)), nil
			},
		},
		types.Command{
			Path:  []string{"addbig"},
			Help:  "Set who your Big is! Required for them to do some things to you.",
			Usage: "!addbig playername",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				bigname := strings.Join(args, " ")
				big, exists := getPlayerByRoomAndName(roomId, bigname)
				if !exists {
					return message(big.Name, getStatus("big_not_playing")), nil
				}
				big.Littles = append(big.Littles, roomId+userId)
				return message(big.Name, getStatus("player_add_big")), nil
			},
		},
		types.Command{
			Path:  []string{"diaper"},
			Help:  "Wrap somepony up in a big, thick diaper! Bigs only!",
			Usage: "!diaper thickness (in cm) playername",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				if len(args) < 2 {
					return message("Syntax is !diaper thickness(cm) littlename"), nil
				}
				if ok := players[roomId+userId].IsBig; !ok {
					return message(getStatus("little_diaper")), nil
				}
				littlename := strings.Join(args[1:], " ")
				little, exists := getPlayerByRoomAndName(roomId, littlename)
				if !exists {
					return message(littlename, getStatus("big_not_playing")), nil
				}
				thickness, _ := strconv.ParseFloat(args[0], 64)
				roundedThickness := int(thickness)
				if thickness <= 0 {
					return message(getStatus("thickness_too_small")), nil
				}
				return little.Diaper(&Diaper{
					Thickness: roundedThickness,
				}), nil
			},
		},
		types.Command{
			Path:  []string{"feed"},
			Help:  "Make a player eat another item. Bigs only!",
			Usage: "!feed itemname to playername",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				if match, _ := regexp.MatchString(".+ to .+", strings.Join(args, " ")); !match {
					return message("Syntax is !feed itemname to littlename."), nil
				}

				// Find the index of "to" in the message
				index := -1
				for i := range args {
					if args[i] == "to" {
						index = i
						break
					}
				}

				// Grab the player name and item name
				itemname := strings.Join(args[:index], " ")
				playername := strings.Join(args[index+1:], " ")
				player, exists := getPlayerByRoomAndName(roomId, playername)
				if !exists {
					return message(playername, " is not playing yet!"), nil
				}
				if players[roomId+userId] == player {
					return message("Use !use to use an item on yourself!"), nil
				}
				if item, ok := items[strings.ToLower(itemname)]; ok {
					return player.ConsumeItem(item), nil
				}
				return message(itemname, " is not a valid item. Create one with !createitem."), nil
			},
		},
		types.Command{
			Path:  []string{"use"},
			Help:  "Use an item on yourself",
			Usage: "!use itemname",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				if len(args) == 0 {
					return message("Syntax is !use itemname"), nil
				}
				upperItemname := strings.Join(args, " ")
				itemname := strings.ToLower(upperItemname)
				player := players[roomId+userId]
				for _, item := range player.Items {
					if item == itemname {
						// Remove item from player's inventory
						player.RemoveItem(itemname)

						return player.ConsumeItem(items[itemname]), nil
					}
				}
				return message("Sorry, you don't seem to have a(n) ", upperItemname,
					" in your inventory."), nil
			},
		},
		types.Command{
			Path:  []string{"listitems"},
			Help:  "List all the known items in this room",
			Usage: "!listitems",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return listItems(roomId), nil
			},
		},
		types.Command{
			Path:  []string{"give"},
			Help:  "Gives the user an item TODO: Make it so you can give items to other players",
			Usage: "!give itemname",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				player := players[roomId+userId]
				upperItemName := strings.Join(args, " ")
				player.Items = append(player.Items, strings.ToLower(upperItemName))
				// TODO: make a status message
				return message("Here's a ", upperItemName, "!"), nil
			},
		},
		types.Command{
			Path:  []string{"createitem"},
			Help:  "Creates a new item with the given status effects",
			Usage: "!createitem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...].",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if len(args) < 3 {
					return message("Syntax is !createitem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...]."), nil
				}
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				newItem := processItem(args)
				items[strings.ToLower(newItem.Name)] = newItem
				itemAttrs, _ := json.MarshalIndent(&newItem, "", " ")
				return message("Item", newItem.Name, getStatus("new_item_added"),
					"\nStats: \n", string(itemAttrs)), nil
			},
		},
		types.Command{
			Path:  []string{"edititem"},
			Help:  "Edit an existing item",
			Usage: "!edititem \"Item Name\" \"It does stuff!\" consumable:true|false [stats...].",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				if len(args) < 2 {
					return message("Syntax is !edititem \"Item Name\" description:\"something\" consumable:true|false [stats...]."), nil
				}
				if _, ok := players[roomId+userId]; !ok {
					return message(getStatus("player_not_playing")), nil
				}
				newItem := processItem(args)
				items[strings.ToLower(newItem.Name)] = newItem
				itemAttrs, _ := json.MarshalIndent(&newItem, "", " ")
				return message("Item", newItem.Name, " has been edited!",
					"\nStats: \n", string(itemAttrs)), nil
			},
		},
		types.Command{
			Path:  []string{"removeitem"},
			Help:  "Remove an item from the room",
			Usage: "!removeitem itemname",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				upperItemname := strings.Join(args, " ")
				itemname := strings.ToLower(upperItemname)
				if _, exists := items[itemname]; !exists {
					return message("I don't know what a ", upperItemname, " is! Check !listitems."), nil
				}
				delete(items, itemname)

				// Delete the item from everyone's list
				for _, player := range players {
					for index, itemId := range player.Items {
						// If the item is in the player's item list, remove it
						if itemId == itemname {
							player.Items = append(player.Items[index:], player.Items[index+1:]...)
							fmt.Println("Deleted, player items are now: ", player.Items)
						}
					}
				}
				return message(getStatus("remove_item")), nil
			},
		},
		types.Command{
			Path:  []string{"myitems"},
			Help:  "List your current items",
			Usage: "!myitems",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				return players[roomId+userId].ListItems(), nil
			},
		},
		types.Command{
			Path:  []string{"stats"},
			Help:  "Show information about your stats",
			Usage: "!stats",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				player := players[roomId+userId]
				stats, _ := json.MarshalIndent(player, "", " ")
				return message(string(stats)), nil
			},
		},
		types.Command{
			Path:  []string{"release"},
			Help:  "Just let go...",
			Usage: "!release",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				player := players[roomId+userId]
				if player.Stats.Bladder == 0 && player.Stats.Bowels == 0 {
					return message(getStatus("player_bladder_bowels_empty")), nil
				}
				player.WetDiaper()
				player.MessDiaper()
				return message(player.Name, getStatus("player_wet_mess")), nil
			},
		},
		types.Command{
			Path:  []string{"hold"},
			Help:  "Try to hold before using the restroom!",
			Usage: "!hold",
			Command: func(roomId, userId string, args []string) (interface{}, error) {
				player := players[roomId+userId]
				player.WetDiaper()
				player.MessDiaper()
				return message(player.Name, getStatus("player_wet_mess")), nil
			},
		},
	}
}

// Expansions
// Currently used to run a method every time someone talks
func (s *Service) Expansions(cli *gomatrix.Client) []types.Expansion {
	return []types.Expansion{
		types.Expansion{
			Regexp: regexp.MustCompile(`.*`),
			Expand: func(roomId, userId string, messageText []string) interface{} {
				return onMessage(roomId, userId, messageText)
			},
		},
	}
}

// message wraps a string in a m.notice gomatrix.TextMessage pointer that can
// be returned to gomatrix to send to the room
func message(text ...interface{}) interface{} {
	return &gomatrix.TextMessage{
		"m.notice",
		fmt.Sprint(text...),
	}
}

// hmessage wraps a string in a m.notice gomatrix.HTMLMessage pointer that can
// be returned to gomatrix to send markdown messages to the room
func hmessage(body, fbody string) interface{} {
	return &gomatrix.HTMLMessage{
		body,
		"m.notice",
		"org.matrix.custom.html",
		fbody,
	}
}

func getStateEvent(cli *gomatrix.Client, roomId, eventType, stateKey string) (stateEvent map[string]string, err error) {
	err = cli.StateEvent(roomId, eventType, stateKey, &stateEvent)
	return
}

// onMessage runs each time a user posts a message in the channel
func onMessage(roomId, userId string, messageText []string) interface{} {
	if player, ok := players[roomId+userId]; ok && !player.IsBig {
		return player.increaseBladderBowels()
	}

	return nil
}

// JSON

// retrieveStoredData parses the userdata.json file for the bot's state since last run
func retrieveStoredData(storageFolder string) {
	playersFile := fmt.Sprint(storageFolder, "/playerdata.json")
	statusFile := fmt.Sprint(storageFolder, "/statusmsgs.json")
	itemsFile := fmt.Sprint(storageFolder, "/items.json")

	// Get players
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(playersFile); exists {

		raw, err := ioutil.ReadFile(playersFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &players)
		if err != nil {
			fmt.Println("Unable to read player data: ", err)
		}
	}

	// Get items
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(itemsFile); exists {

		raw, err := ioutil.ReadFile(itemsFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &items)
		if err != nil {
			fmt.Println("Unable to read item data: ", err)
		}
	}

	// Get status messages
	// Return if file does not exist yet (will be created on shutdown)
	if exists, _ := exists(statusFile); exists {

		raw, err := ioutil.ReadFile(statusFile)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		err = json.Unmarshal(raw, &statusMsgs)
		if err != nil {
			fmt.Println("Unable to read status message data: ", err)
		}
	}

	// Initialize the data maps if not already
	if players == nil {
		players = make(map[string]*Player)
	}
	if items == nil {
		items = make(map[string]*Item)
	}
	if statusMsgs == nil {
		statusMsgs = make(map[string]*StatusMsg)
	}
}

func storeData(storageFolder string) {
	playersFile := fmt.Sprint(storageFolder, "/playerdata.json")
	itemsFile := fmt.Sprint(storageFolder, "/items.json")

	// Backup player and item data
	os.Link(playersFile, playersFile+".backup")
	os.Link(itemsFile, itemsFile+".backup")

	// Store player information
	playerdata, _ := json.MarshalIndent(&players, "", " ")

	err := ioutil.WriteFile(playersFile, playerdata, 0644)
	if err != nil {
		fmt.Println("Error writing player data: ", err)
	}

	// Store item information
	itemdata, _ := json.MarshalIndent(&items, "", " ")

	err = ioutil.WriteFile(itemsFile, itemdata, 0644)
	if err != nil {
		fmt.Println("Error writing item data: ", err)
	}
}

// exists returns whether the given file or directory exists or not
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func init() {
	storageFolder := "src/github.com/matrix-org/go-neb/services/dndrp"

	types.RegisterService(func(serviceId, serviceUserId, webhookEndpointURL string) types.Service {
		service = &Service{
			DefaultService: types.NewDefaultService(serviceId, serviceUserId, ServiceType),
		}
		return service
	})

	// Write data on bot shutdown
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		// TODO: Ask neb developers if this is a clean shutdown...
		fmt.Println("Shutting down cleanly...")
		storeData(storageFolder)
		os.Exit(1)
	}()

	retrieveStoredData(storageFolder)
}
